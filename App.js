/* eslint-disable global-require */
import { AppLoading } from 'expo';
import { Asset } from 'expo-asset';
import * as Font from 'expo-font';
import React, { useState } from 'react';
import {
  Platform,
  StatusBar,
  StyleSheet,
  View
} from 'react-native';
import { Provider as PaperProvider, DefaultTheme } from 'react-native-paper';
import { Ionicons } from '@expo/vector-icons';

import AppNavigator from './navigation/AppNavigator';
import Colors from './constants/Colors';
import { StateProvider } from './components/context/user_context';

const theme = {
  ...DefaultTheme,
  roundness: 8,
  colors: {
    ...DefaultTheme.colors,
    primary: Colors.deepPink,
    accent: Colors.white,
    background: Colors.white,
  },
  fonts: {
    regular: 'dosis-regular',
    medium: 'dosis-medium',
    light: 'dosis-light',
    thin: 'dosis-extralight',
  }
};

export default function App(props) {
  const [isLoadingComplete, setLoadingComplete] = useState(false);
  const { skipLoadingScreen } = props;

  const initialState = {
    isLogged: false,
    isAsso: undefined,
    token: '',
    user: undefined,
  };

  const reducer = (state, action) => {
    switch (action.type) {
      case 'isLogged':
        return ({
          ...state,
          isLogged: action.value
        });
      case 'isAsso':
        return ({
          ...state,
          isAsso: action.value
        });
      case 'user':
        return ({
          ...state,
          user: action.value,
          isLogged: action.value !== undefined,
        });
      case 'resetState':
        return initialState;
      default:
        return state;
    }
  };

  if (!isLoadingComplete && !skipLoadingScreen) {
    return (
      <AppLoading
        startAsync={loadResourcesAsync}
        onFinish={() => handleFinishLoading(setLoadingComplete)}
      />
    );
  }
  return (
    <PaperProvider theme={theme}>
      <StateProvider initialState={initialState} reducer={reducer}>
        <View style={styles.container}>
          {Platform.OS === 'ios' && <StatusBar barStyle="default" />}
          <AppNavigator />
        </View>
      </StateProvider>
    </PaperProvider>
  );
}

async function loadResourcesAsync() {
  await Promise.all([
    Asset.loadAsync([
      require('./assets/images/bonmood.png'),
    ]),
    Font.loadAsync({
      // This is the font that we are using for our tab bar
      ...Ionicons.font,
      'dosis-extralight': require('./assets/fonts/Dosis-ExtraLight.ttf'),
      'dosis-light': require('./assets/fonts/Dosis-Light.ttf'),
      'dosis-regular': require('./assets/fonts/Dosis-Regular.ttf'),
      'dosis-medium': require('./assets/fonts/Dosis-Medium.ttf'),
      'dosis-semibold': require('./assets/fonts/Dosis-SemiBold.ttf'),
      'dosis-bold': require('./assets/fonts/Dosis-Bold.ttf'),
      'dosis-extrabold': require('./assets/fonts/Dosis-ExtraBold.ttf'),
    }),
  ]);
}

function handleFinishLoading(setLoadingComplete) {
  setLoadingComplete(true);
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
});
