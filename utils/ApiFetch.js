import API from '../constants/Api';

const FetchApi = async (route, method = 'GET', body, isExtern = false, data = 'data') => {
  const response = await fetch(`${isExtern ? '' : API}${route}`, {
    method,
    body: body ? JSON.stringify(body) : null,
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json',
    },
  });
  let jsonResponse;
  try {
    jsonResponse = await response.json();
  } catch (error) {
    throw new Error('Something went wrong.');
  }
  if (response.ok) {
    return jsonResponse[data];
  }
  throw new Error(jsonResponse.message);
};

export default FetchApi;
