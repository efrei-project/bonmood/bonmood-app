import React from 'react';
import { TextInput } from 'react-native-paper';

const InputText = props => (
  <TextInput
    {...props}
    theme={{
      colors: {
        background: 'transparent',
      },
    }}
    style={{
      marginTop: 16.0,
    }}
  />
);

export default InputText;
