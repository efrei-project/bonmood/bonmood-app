import React, { useState } from 'react';
import { Alert } from 'react-native';
import { withNavigation } from 'react-navigation';
import Icon from 'react-native-vector-icons/MaterialIcons';
import {
  Card,
  Caption,
  Title,
  Dialog,
  Portal,
  Button
} from 'react-native-paper';
import moment from 'moment';
import Row from './Row';
import AvaibilitieDialog from './AvailabilityDialog';
import styles from '../constants/Styles';

const AvaibilitieRow = ({ available }) => {
  const [visible, setVisible] = useState();
  const showDialog = () => setVisible(true);
  const hideDialog = () => setVisible(false);

  const deleteAvailability = () => {
    Alert.alert(
      'Delete availability',
      'Are you sure to delete this availability ?',
      [
        {
          text: 'Cancel',
          onPress: () => console.log('Cancel Pressed'),
        },
        {
          text: 'Confirm',
          // TO DO request api for delete availability
          onPress: () => console.log('OK Pressed')
        },
      ],
      {
        cancelable: false
      },
    );
  };

  const editAvailability = () => (
    <Portal>
      <Dialog visible={visible}>
        <AvaibilitieDialog />
        <Dialog.Actions>
          <Button onPress={hideDialog}>Cancel</Button>
          <Button onPress={hideDialog}>Done</Button>
        </Dialog.Actions>
      </Dialog>
    </Portal>
  );

  return (
    <Card
      style={styles.card}
      elevation={3}
    >
      <Card.Content>
        <Row justifyContent="space-between">
          <Row>
            <Icon name="event-available" size={20} style={styles.icon} />
            <Title>{moment(available.date).format('dddd Do MMMM')}</Title>
          </Row>
          <Icon
            name="create"
            size={20}
            style={styles.icon}
            onPress={showDialog}
          />
          { editAvailability() }
        </Row>
        <Row justifyContent="space-between">
          <Row>
            <Icon name="query-builder" size={20} style={styles.icon} />
            <Caption>
              {moment(available.hour_begin).format('h:mm')}
              -
              {moment(available.hour_end).format('h:mm')}
            </Caption>
          </Row>
          <Icon
            name="delete"
            size={20}
            style={styles.icon}
            onPress={() => deleteAvailability()}
          />
        </Row>
      </Card.Content>
    </Card>
  );
};

export default withNavigation(AvaibilitieRow);
