import React, { useState } from 'react';
import {
  View,
  Text,
  ScrollView,
  Picker
} from 'react-native';
import { withNavigation } from 'react-navigation';
import { Button, HelperText } from 'react-native-paper';
import DatePicker from 'react-native-datepicker';
import moment from 'moment';
import useInput from './hooks/useInput';
import styles from '../constants/Colors';
import InputText from './InputText';
import Row from './Row';
import FetchApi from '../utils/ApiFetch';
import { useStateValue } from './context/user_context';

const RegisterForm = ({ navigation }) => {
  const [{ isAsso }, dispatch] = useStateValue();

  const gender = useInput();
  const birthDate = useInput(moment());
  const lastname = useInput();
  const firstname = useInput();
  const association = useInput();
  const responsable = useInput();
  const email = useInput();
  const password = useInput();

  const [error, setError] = useState('');

  const registerUser = async () => {
    try {
      const data = {
        email: email.value,
        password: password.value,
      };

      const dataBenevole = {
        ...data,
        firstname: firstname.value,
        lastname: lastname.value,
        gender: gender.value,
        birthday: birthDate.value
      };

      const dataAssociation = {
        ...data,
        name: association.value,
        manager: responsable.value,
      };

      const userData = await FetchApi(
        isAsso ? '/associations/create' : '/volunteers/create',
        'POST',
        isAsso ? dataAssociation : dataBenevole
      );

      if (!userData) {
        setError('Unknown error');
        return false;
      }
      dispatch({ type: 'user', value: userData });
      setError('');
      return true;
    } catch (e) {
      setError(e);
      return false;
    }
  };

  const onRegisterPressed = async () => {
    const isSuccess = await registerUser();
    if (!isSuccess) return;

    if (isAsso) navigation.navigate('MainTabNavigator');
    else navigation.goBack();
  };

  return (
    <ScrollView style={styles.container}>
      <HelperText type="error" visible={error}>
        {`${error}`}
      </HelperText>
      {isAsso
        ? (
          <View>
            <InputText
              label="Association Name"
              name="association"
              placeholder="Enter your association"
              value={association.value}
              onChangeText={text => association.setValue(text)}
            />
            <InputText
              label="Responsable Name"
              name="responsable"
              placeholder="Enter name of responsable"
              value={responsable.value}
              onChangeText={text => responsable.setValue(text)}
            />
            <InputText
              label="Email"
              name="email"
              placeholder="Enter a email"
              value={email.value}
              onChangeText={text => email.setValue(text)}
            />
            <InputText
              label="Password"
              name="password"
              placeholder="Enter a password"
              value={password.value}
              onChangeText={text => password.setValue(text)}
              secureTextEntry
            />
          </View>
        ) : (
          <View>
            <Row>
              <View>
                <Text>Gender</Text>
                <Picker
                  selectedValue={gender.value}
                  style={{ height: 50, width: 120 }}
                  onValueChange={value => gender.setValue(value)}
                >
                  <Picker.Item label="Female" value="female" />
                  <Picker.Item label="Male" value="male" />
                  <Picker.Item label="Other" value="other" />
                </Picker>
              </View>
              <View>
                <Text> Birth date</Text>
                <DatePicker
                  style={{ width: 200 }}
                  date={birthDate.value}
                  mode="date"
                  placeholder="select date"
                  format="YYYY-MM-DD"
                  minDate="1970-01-01"
                  maxDate={new Date()}
                  confirmBtnText="Confirm"
                  cancelBtnText="Cancel"
                  customStyles={{
                    dateIcon: {
                      position: 'absolute',
                      right: 0,
                      top: 4,
                      marginRight: 0
                    },
                    dateInput: {
                      marginRight: 36
                    }
                  }}
                  onDateChange={date => birthDate.setValue(date)}
                />
              </View>
            </Row>
            <InputText
              label="Last Name"
              name="lastName"
              placeholder="Enter your lastname"
              value={lastname.value}
              onChangeText={text => lastname.setValue(text)}
            />
            <InputText
              label="First Name"
              name="firstname"
              placeholder="Enter your firstname"
              value={firstname.value}
              onChangeText={text => firstname.setValue(text)}
            />
            <InputText
              label="Email"
              name="email"
              placeholder="Enter a email"
              value={email.value}
              onChangeText={text => email.setValue(text)}
            />
            <InputText
              label="Password"
              name="password"
              placeholder="Enter a password"
              value={password.value}
              onChangeText={text => password.setValue(text)}
              secureTextEntry
            />
          </View>
        )
      }
      <HelperText
        type="error"
        visible={password.value.length > 0 && password.value.length < 7}
      >
        Your password have to contain 7 caracters
      </HelperText>
      <Button
        style={styles.button}
        onPress={onRegisterPressed}
        mode="contained"
        dark
      >
        Register
      </Button>
    </ScrollView>
  );
};

export default withNavigation(RegisterForm);
