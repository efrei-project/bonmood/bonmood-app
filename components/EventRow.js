import React, { useState, useEffect } from 'react';
import { Alert } from 'react-native';
import { withNavigation } from 'react-navigation';
import {
  Card,
  Caption,
  Title,
  Paragraph,
  Button,
} from 'react-native-paper';
import Icon from 'react-native-vector-icons/MaterialIcons';
import moment from 'moment';
import Row from './Row';
import styles from '../constants/Styles';
import { useStateValue } from './context/user_context';
import FetchApi from '../utils/ApiFetch';

const EventRow = ({ event, navigation }) => {
  const [{ isAsso }] = useStateValue();
  const [participant, setParticipant] = useState({
    number: 0
  });

  const persons = `${participant.number} / ${event.number}`;

  const deleteEventApi = async () => {
    try {
      await FetchApi(`/events/delete/${event.id}`, 'DELETE', {});
      Alert.alert('Succès', 'Evenement correctement supprimé');
    } catch (e) {
      Alert.alert('Error', `${e}`);
    }
  };

  useEffect(() => {
    getParticipant();
  }, []);

  const getParticipant = async () => {
    const res = await FetchApi(`/events/${event.id}/participant`, 'GET');
    setParticipant({ number: res.length });
  };

  const deleteEvent = () => {
    Alert.alert(
      'Delete event',
      `Are you sure to delete this ${event.name} ?`,
      [
        {
          text: 'Cancel',
        },
        {
          text: 'Confirm',
          onPress: () => deleteEventApi(),
        },
      ],
      {
        cancelable: false
      },
    );
  };

  if (isAsso) {
    return (
      <Card style={styles.card} elevation={3}>
        <Card.Content>
          <Row justifyContent="space-between">
            <Title>{event.name}</Title>
            <Icon name="delete" size={20} onPress={() => deleteEvent()} />
          </Row>
          <Row justifyContent="space-between">
            <Paragraph>{event.description}</Paragraph>
            <Button onPress={() => navigation.navigate('EventScreen', { event })}>
              Details
            </Button>
          </Row>
        </Card.Content>
      </Card>
    );
  }
  return (
    <Card
      style={styles.card}
      elevation={3}
      onPress={() => navigation.navigate('EventScreen', { event })}
    >
      <Card.Content>
        <Title>{event.name}</Title>
        <Paragraph>{event.description}</Paragraph>
        <Row justifyContent="space-between">
          <Caption>{persons}</Caption>
          <Caption>{moment(event.dateBegin).format('LLL')}</Caption>
        </Row>
      </Card.Content>
    </Card>
  );
};

export default withNavigation(EventRow);
