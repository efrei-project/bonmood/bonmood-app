import React from 'react';
import { Appbar } from 'react-native-paper';

const AppBar = ({ title, children }) => (
  <Appbar.Header dark>
    <Appbar.Content title={title} />
    { children }
  </Appbar.Header>
);

export default AppBar;
