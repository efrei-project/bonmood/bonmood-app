import React, { useState } from 'react';
import { View } from 'react-native';
import { withNavigation } from 'react-navigation';
import { Button, HelperText } from 'react-native-paper';
import useInput from './hooks/useInput';
import styles from '../constants/Styles';
import InputText from './InputText';
import FetchApi from '../utils/ApiFetch';
import { useStateValue } from './context/user_context';

const LoginForm = ({ navigation }) => {
  const [{ isAsso }, dispatch] = useStateValue();
  const email = useInput();
  const password = useInput();

  const [error, setError] = useState('');

  const loginUser = async () => {
    try {
      const userData = await FetchApi(
        isAsso ? '/associations/login' : '/volunteers/login',
        'POST',
        { email: email.value, password: password.value }
      );
      if (!userData) {
        setError('Unknown error');
        return false;
      }
      dispatch({ type: 'user', value: userData });
      setError('');
      return true;
    } catch (e) {
      setError(e);
      return false;
    }
  };

  const onLoginPressed = async () => {
    const isSuccess = await loginUser();
    if (!isSuccess) return;

    if (isAsso) {
      navigation.navigate('MainTabNavigator');
    } else {
      navigation.goBack();
    }
  };

  // TODO verify with data bdd
  return (
    <View style={styles.container}>
      <HelperText type="error" visible={error}>
        {`${error}`}
      </HelperText>
      <InputText
        label="Email"
        name="email"
        placeholder="Enter a email"
        value={email.value}
        onChangeText={email.setValue}
      />
      <InputText
        label="Password"
        name="password"
        placeholder="Enter a password"
        value={password.value}
        onChangeText={password.setValue}
        secureTextEntry
      />
      <Button style={styles.button} onPress={onLoginPressed} mode="contained" dark>
        Login
      </Button>
    </View>
  );
};

export default withNavigation(LoginForm);
