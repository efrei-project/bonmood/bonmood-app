import React from 'react';
import { View, Text } from 'react-native';
import { Dialog } from 'react-native-paper';
import moment from 'moment';
import Row from './Row';
import useInput from './hooks/useInput';
import styles from '../constants/Styles';
import InputText from './InputText';

const UserDialog = () => {
  const lastname = useInput();
  const firstname = useInput();
  const email = useInput();

  return (
    <View>
      <Dialog.Title>Edit profile</Dialog.Title>
      <Dialog.Content style={styles.dialogContent}>
        <Row justifyContent="space-between">
          <View>
            <InputText
              label="Last Name"
              name="lastname"
              placeholder="Enter new lastname"
              value={lastname.value}
              onChangeText={text => lastname.setValue(text)}
            />
          </View>
          <View>
            <InputText
              label="First Name"
              name="first"
              placeholder="Enter new firstname"
              value={firstname.value}
              onChangeText={text => firstname.setValue(text)}
            />
          </View>
        </Row>
        <View style={{ marginTop: 16.0 }}>
          <InputText
            label="Email"
            name="email"
            placeholder="Enter a email"
            value={email.value}
            onChangeText={text => email.setValue(text)}
            />
        </View>
      </Dialog.Content>
    </View>
  );
};

export default UserDialog;
