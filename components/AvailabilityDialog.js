import React from 'react';
import { View, Text } from 'react-native';
import { Dialog } from 'react-native-paper';
import moment from 'moment';
import DatePicker from 'react-native-datepicker';
import Row from './Row';
import useInput from './hooks/useInput';
import styles from '../constants/Styles';

const AvaibilitieDialog = () => {
  const hourBegin = useInput(moment());
  const hourEnd = useInput(moment().add(2, 'hours'));
  const date = useInput(moment());

  return (
    <View>
      <Dialog.Title>Edit availability</Dialog.Title>
      <Dialog.Content style={styles.dialogContent}>
        <Row justifyContent="space-between">
          <View>
            <Text>Start time </Text>
            <DatePicker
              style={{ width: 100 }}
              date={hourBegin.value}
              mode="time"
              placeholder="Select time"
              format="HH:mm"
              confirmBtnText="Confirm"
              cancelBtnText="Cancel"
              customStyles={{
                dateIcon: {
                  position: 'absolute',
                  right: 0,
                  top: 4,
                  marginRight: 0
                },
                dateInput: {
                  marginRight: 36
                }
              }}
              onDateChange={newDate => hourBegin.setValue(newDate)}
            />
          </View>
          <View>
            <Text>End time </Text>
            <DatePicker
              style={{ width: 100 }}
              date={hourEnd.value}
              mode="time"
              placeholder="Select time"
              format="HH:mm"
              confirmBtnText="Confirm"
              cancelBtnText="Cancel"
              customStyles={{
                dateIcon: {
                  position: 'absolute',
                  right: 0,
                  top: 4,
                  marginRight: 0
                },
                dateInput: {
                  marginRight: 36
                }
              }}
              onDateChange={newDate => hourEnd.setValue(newDate)}
            />
          </View>
        </Row>
        <View style={{ marginTop: 16.0 }}>
          <Text>Date</Text>
          <DatePicker
            style={{ width: 150 }}
            date={date.value}
            mode="date"
            placeholder="Select date"
            format="YYYY-MM-DD"
            minDate={new Date()}
            maxDate={moment().add(2, 'years').toDate()}
            confirmBtnText="Confirm"
            cancelBtnText="Cancel"
            customStyles={{
              dateIcon: {
                position: 'absolute',
                right: 0,
                top: 4,
                marginRight: 0
              },
              dateInput: {
                marginRight: 36
              }
            }}
            onDateChange={newDate => date.setValue(newDate)}
          />
        </View>
      </Dialog.Content>
    </View>
  );
};

export default AvaibilitieDialog;
