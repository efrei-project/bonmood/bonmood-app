import { StyleSheet } from 'react-native';
import Colors from './Colors';
import Layout from './Layout';

export default StyleSheet.create({
  container: {
    flex: 1,
  },
  centered: {
    alignItems: 'center',
    justifyContent: 'center'
  },
  center: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  },
  header: {
    paddingTop: 50,
    paddingBottom: 20,
    paddingHorizontal: 16,
    alignItems: 'center',
  },
  image: {
    height: 100,
    marginBottom: 16,
  },
  headerImage: {
    height: Layout.window.height * 0.33,
    width: '100%',
    backgroundColor: 'transparent'
  },
  title: {
    fontSize: 22.0,
    width: '100%',
    textAlign: 'center',
    fontFamily: 'dosis-bold',
  },
  textComponent: {
    borderRadius: 5,
    borderWidth: 1,
    borderColor: Colors.lightPink,
    color: Colors.black,
    backgroundColor: 'transparent',
    padding: 2,
    textAlign: 'auto',
    margin: 10
  },
  card: {
    marginHorizontal: 16.0,
    marginVertical: 8.0,
  },
  icon: {
    marginRight: 12.0
  },
  dialog: {
    height: 200
  },
  dialogContent: {
    height: 150
  },
  space: {
    marginTop: 16.0,
  },
  button: {
    marginTop: 16.0,
    width: 120,
    backgroundColor: Colors.deepPink,
  },
  tabbar: {
    backgroundColor: 'transparent'
  },
  tablabel: {
    color: Colors.black
  },
  tabindicator: {
    backgroundColor: Colors.deepPink
  },
  fab: {
    position: 'absolute',
    right: 16,
    bottom: 16,
    backgroundColor: Colors.deepPink,
  },
  fabEdit: {
    position: 'absolute',
    right: 100,
    bottom: 16,
    backgroundColor: Colors.deepPink,
  },
});
