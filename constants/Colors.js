export default {
  lightPink: '#FFC2E9',
  salmon: '#F4D5DD',
  corail: '#E598A3',
  deepPink: '#D36781',
  cream: '#FFFAF3',
  white: '#FFFFFF',
  black: '#000000',
};
