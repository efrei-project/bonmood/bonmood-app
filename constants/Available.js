const AvailableData = [
  {
    id: 1,
    date: '2019-06-16',
    hour_begin: '2019-06-17T10:00:00',
    hour_end: '2019-06-17T16:00:00',
  },
  {
    id: 2,
    date: '2019-06-17',
    hour_begin: '2019-06-17T10:00:00',
    hour_end: '2019-06-17T16:00:00',
  },
  {
    id: 3,
    date: '2019-06-18',
    hour_begin: '2019-06-17T10:00:00',
    hour_end: '2019-06-17T16:00:00',
  },
  {
    id: 4,
    date: '2019-06-19',
    hour_begin: '2019-06-17T10:00:00',
    hour_end: '2019-06-17T16:00:00',
  },
  {
    id: 5,
    date: '2019-06-20',
    hour_begin: '2019-06-17T10:00:00',
    hour_end: '2019-06-17T16:00:00',
  },
];

export default AvailableData;
