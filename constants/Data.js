const EventsData = [
  {
    id: 1,
    title: 'Evenement 1',
    description: 'Une super description',
    date: '2019-07-17T16:12:00',
    specification: 'none',
    place: 'Paris',
    persons: 5,
    totalPersons: 15,
    position: {
      latitude: 48.837364,
      longitude: 2.349923,
    },
    image: 'https://img.aws.la-croix.com/2019/02/26/1201005077/volontaire-Restos-coeur-prepare-distribution-nourriture-22-novembre-2016_0_728_466.jpg',
  },
  {
    id: 2,
    title: 'Evenement 2',
    description: 'Une super description',
    date: '2019-06-18T16:12:00',
    specification: 'none',
    place: 'Paris',
    persons: 5,
    totalPersons: 15,
    position: {
      latitude: 48.850174,
      longitude: 2.385918,
    },
    image: 'https://img.aws.la-croix.com/2019/02/26/1201005077/volontaire-Restos-coeur-prepare-distribution-nourriture-22-novembre-2016_0_728_466.jpg',
  },
  {
    id: 3,
    title: 'Evenement 3',
    description: 'Une super description',
    date: '2019-06-19T16:12:00',
    specification: 'none',
    place: 'Paris',
    persons: 5,
    totalPersons: 15,
    position: {
      latitude: 48.870417,
      longitude: 2.344188,
    },
    image: 'https://img.aws.la-croix.com/2019/02/26/1201005077/volontaire-Restos-coeur-prepare-distribution-nourriture-22-novembre-2016_0_728_466.jpg',
  },
  {
    id: 4,
    title: 'Evenement 4',
    description: 'Une super description',
    date: '2019-06-15T16:12:00',
    specification: 'none',
    place: 'Paris',
    persons: 5,
    totalPersons: 15,
    position: {
      latitude: 48.876139,
      longitude: 2.306695,
    },
    image: 'https://img.aws.la-croix.com/2019/02/26/1201005077/volontaire-Restos-coeur-prepare-distribution-nourriture-22-novembre-2016_0_728_466.jpg',
  },
  {
    id: 5,
    title: 'Evenement 5',
    description: 'Une super description',
    date: '2019-06-16T16:12:00',
    specification: 'none',
    place: 'Paris',
    persons: 5,
    totalPersons: 15,
    position: {
      latitude: 48.840094,
      longitude: 2.308843,
    },
    image: 'https://img.aws.la-croix.com/2019/02/26/1201005077/volontaire-Restos-coeur-prepare-distribution-nourriture-22-novembre-2016_0_728_466.jpg',
  },
];

export default EventsData;
