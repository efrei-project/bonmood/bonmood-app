/* eslint-disable react/no-unused-state */
import React, { useState, useEffect } from 'react';
import { BottomNavigation, Appbar } from 'react-native-paper';
import { View } from 'react-native';
import HomeScreen from '../screens/HomeScreen';
import MapScreen from '../screens/MapScreen';
import CalendarScreen from '../screens/CalendarScreen';
import AvailableScreen from '../screens/AvailableScreen';
import Colors from '../constants/Colors';
import styles from '../constants/Styles';
import AppBar from '../components/AppBar';
import { useStateValue } from '../components/context/user_context';
import ProfileScreenAssociation from '../screens/ProfileScreenAssociation';
import AddEventScreen from '../screens/AddEventScreen';

const MainTabNavigator = ({ navigation }) => {
  const [{ isAsso }] = useStateValue();

  const assoRoutes = [
    { key: 'home', title: 'Home', icon: 'home' },
    { key: 'calendar', title: 'Calendar', icon: 'today' },
    { key: 'profile', title: 'Profile', icon: 'account-circle' },
    { key: 'add', title: 'Event', icon: 'add' },
  ];

  const benevRoutes = [
    { key: 'home', title: 'Home', icon: 'home' },
    { key: 'map', title: 'Map', icon: 'map' },
    { key: 'calendar', title: 'Calendar', icon: 'today' },
    { key: 'available', title: 'Available', icon: 'access-time' },
  ];

  const [state, setState] = useState({
    index: 0,
    routes: isAsso ? assoRoutes : benevRoutes,
  });

  useEffect(() => {
    setState({
      ...state,
      routes: isAsso ? assoRoutes : benevRoutes,
    });
  }, [isAsso]);

  const renderScene = BottomNavigation.SceneMap({
    home: HomeScreen,
    profile: ProfileScreenAssociation,
    map: MapScreen,
    calendar: CalendarScreen,
    available: AvailableScreen,
    add: AddEventScreen
  });

  const handleIndexChange = index => setState({ ...state, index });
  const { index, routes } = state;
  const { title } = routes[index];

  return (
    <View style={styles.container}>
      {
        (isAsso && index !== 3) || (!isAsso && index !== 1)
          ? (
            <AppBar title={title}>
              {isAsso ? (null) : (
                <Appbar.Action icon="account-circle" onPress={() => navigation.navigate('User')} />
              )}
            </AppBar>
          )
          : null
      }
      <BottomNavigation
        navigationState={state}
        onIndexChange={handleIndexChange}
        renderScene={renderScene}
        getColor={() => Colors.white}
      />
    </View>
  );
};

export default MainTabNavigator;
