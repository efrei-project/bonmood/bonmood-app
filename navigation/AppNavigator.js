import { createAppContainer, createSwitchNavigator, createStackNavigator } from 'react-navigation';
import WelcomeScreen from '../screens/WelcomeScreen';
import EventScreen from '../screens/EventScreen';
import MainTabNavigator from './MainTabNavigator';
import LoginScreen from '../screens/LoginScreen';
import SplashScreen from '../screens/SplashScreen';
import UserScreen from '../screens/UserScreen';
import AddEventScreen from '../screens/AddEventScreen';

export default createAppContainer(
  createSwitchNavigator({
    Splash: SplashScreen,
    Main: createStackNavigator({
      MainTabNavigator: {
        screen: MainTabNavigator,
        navigationOptions: { header: null },
      },
      EventScreen: {
        screen: EventScreen,
        navigationOptions: { header: null }
      },
      Welcome: {
        screen: WelcomeScreen,
        navigationOptions: { header: null },
      },
      Login: {
        screen: LoginScreen,
        navigationOptions: { header: null },
      },
      User: {
        screen: UserScreen,
        navigationOptions: { header: null },
      },
      AddEvent: {
        screen: AddEventScreen,
        navigationOptions: { header: null }
      }
    }),
  })
);
