import React, { useState, useEffect } from 'react';
import MapView from 'react-native-maps';
import { withNavigation } from 'react-navigation';
import { ActivityIndicator, Text } from 'react-native-paper';
import styles from '../constants/Styles';
import FetchApi from '../utils/ApiFetch';
import Colors from '../constants/Colors';

const MapScreen = ({ navigation }) => {
  const [events, setEvents] = useState({
    isLoading: true,
    data: [],
    error: undefined
  });

  useEffect(() => {
    const fetchEvent = async () => {
      try {
        const data = await FetchApi('/events');
        console.log(data);

        setEvents({ isLoading: false, data });
      } catch (error) {
        setEvents({ isLoading: false, error });
      }
    };
    fetchEvent();
  }, []);

  const { isLoading, data, error } = events;

  if (isLoading) {
    return <ActivityIndicator size="large" color={Colors.lightPink} style={styles.center} />;
  }
  if (error) {
    // eslint-disable-next-line react/jsx-one-expression-per-line
    return <Text>`Error when try to get events... ${JSON.stringify(error)}`</Text>;
  }

  return (
    <MapView style={{ flex: 1 }} showsUserLocation>
      {data.map(event => (
        <MapView.Marker
          key={event.id}
          coordinate={{
            latitude: event.latitude,
            longitude: event.longitude,
          }}
          title={event.name}
          description={event.description}
          onPress={() => {
            navigation.navigate('EventScreen', { event });
          }}
        />
      ))}
    </MapView>
  );
};

export default withNavigation(MapScreen);
