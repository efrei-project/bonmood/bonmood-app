/* eslint-disable global-require */
import React, { Component } from 'react';
import {
  View,
  Image,
  ActivityIndicator
} from 'react-native';
import Colors from '../constants/Colors';
import styles from '../constants/Styles';

export default class SplashScreen extends Component {
  componentDidMount() {
    // TODO: Fetch data
    this.switchScreen();
  }

  delay = ms => new Promise(res => setTimeout(res, ms));

  switchScreen = async () => {
    await this.delay(1000);
    const { navigation } = this.props;
    navigation.navigate('Welcome');
  };

  render() {
    return (
      <View style={styles.container}>
        <View style={styles.container} />
        <View style={styles.center}>
          <Image source={require('../assets/images/heart.png')} />
        </View>
        <View style={styles.center}>
          <ActivityIndicator size="large" color={Colors.lightPink} />
        </View>
      </View>
    );
  }
}
