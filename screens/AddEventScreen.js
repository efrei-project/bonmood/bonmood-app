/* eslint-disable react/jsx-one-expression-per-line */
import React, { useEffect, useState } from 'react';
import {
  KeyboardAvoidingView,
  ScrollView,
  View,
  Text,
  Image,
  TouchableHighlight,
  TouchableOpacity,
  Alert,
} from 'react-native';
import { withNavigation } from 'react-navigation';
import moment from 'moment';
import DatePicker from 'react-native-datepicker';
import { Button } from 'react-native-paper';
import { ImagePicker, Permissions, Constants } from 'expo';
import Autocomplete from 'react-native-autocomplete-input';
import useInput from '../components/hooks/useInput';
import Row from '../components/Row';
import InputText from '../components/InputText';
import Styles from '../constants/Styles';
import FetchApi from '../utils/ApiFetch';
import { useStateValue } from '../components/context/user_context';

const AddEventScreen = ({ navigation }) => {
  const [{ user }] = useStateValue();

  const image = useInput('https://carepharmaceuticals.com.au/wp-content/uploads/sites/19/2018/02/placeholder-600x400.png');
  const title = useInput();
  const persons = useInput(10);

  const [geocoderData, setGeocoderData] = useState([]);
  const [geocoder, setGeocoder] = useState();
  const [geocoderQuery, setGeocoderQuery] = useState();

  const description = useInput();
  const [dateBegin, setDateBegin] = useState(moment());
  const [dateEnd, setDateEnd] = useState(moment().add(2, 'hours'));

  const createEvent = async () => {
    try {
      const newEvent = await FetchApi('/events/create', 'POST', {
        name: title.value,
        place: geocoder.properties.label,
        number: persons.value,
        dateBegin,
        dateEnd,
        description: description.value,
        associationId: user.id,
        latitude: geocoder.geometry.coordinates[1],
        longitude: geocoder.geometry.coordinates[0],
        tags: []
      });
      if (!newEvent) {
        Alert.alert('Error', 'Unknown error');
        return false;
      }
      return true;
    } catch (e) {
      Alert.alert('Error', `${e}`);
      return false;
    }
  };

  const searchAddress = async (query) => {
    try {
      const addresses = await FetchApi(
        `https://api-adresse.data.gouv.fr/search/?q=${query}`,
        'GET',
        null,
        true,
        'features'
      );
      setGeocoderData(addresses);
    } catch (e) {
      Alert.alert('Error', `${e}`);
    }
  };

  const onValidatePressed = async () => {
    const isSuccess = await createEvent();
    if (!isSuccess) return;
    navigation.navigate('MainTabNavigator');
  };

  useEffect(() => {
    getPermissionAsync();
  }, []);

  const getPermissionAsync = async () => {
    if (Constants.platform.ios) {
      const { status } = await Permissions.askAsync(Permissions.CAMERA_ROLL);
      if (status !== 'granted') {
        Alert.alert('Sorry, we need camera roll permissions to make this work!');
      }
    }
  };

  const pickImage = async () => {
    const result = await ImagePicker.launchImageLibraryAsync({
      mediaTypes: ImagePicker.MediaTypeOptions.All,
      allowsEditing: true,
      aspect: [4, 3],
    });

    if (!result.cancelled) {
      image.setValue(result.uri);
    }
  };

  return (
    <KeyboardAvoidingView style={Styles.container} behavior="padding" enabled>
      <ScrollView style={Styles.container}>
        <TouchableHighlight onPress={pickImage}>
          <Image
            source={{ uri: image.value }}
            style={Styles.headerImage}
            justifyContent="center"
          />
        </TouchableHighlight>

        <InputText
          label="Title"
          name="title"
          placeholder="Enter title event"
          value={title.value}
          onChangeText={title.setValue}
        />
        <InputText
          name="description"
          multiline
          numberOfLines={7}
          placeholder="Enter your description"
          value={description.value}
          onChangeText={description.setValue}
          style={Styles.space}
        />
        <InputText
          label="Number of people"
          name="persons"
          placeholder="Enter the maximum number of people"
          value={`${persons.value}`}
          onChangeText={persons.setValue}
          style={Styles.space}
        />
        <Row style={Styles.space} justifyContent="space-between">
          <View style={{ flex: 1 }}>
            <Text>Start</Text>
            <DatePicker
              date={dateBegin}
              mode="datetime"
              placeholder="Select begin date time"
              format="MMMM Do YYYY, HH:mm"
              confirmBtnText="Confirm"
              cancelBtnText="Cancel"
              onDateChange={date => setDateBegin(date)}
              style={{ width: '100%' }}
              customStyles={{
                dateIcon: {
                  position: 'absolute',
                  left: 0,
                  top: 4,
                  marginLeft: 0
                },
                dateInput: {
                  marginLeft: 42
                }
              }}
            />
          </View>
          <View style={{ width: 24 }} />
          <View style={{ flex: 1 }}>
            <Text>End time</Text>
            <DatePicker
              date={dateEnd}
              mode="datetime"
              placeholder="Select end date time"
              format="MMMM Do YYYY, HH:mm"
              confirmBtnText="Confirm"
              cancelBtnText="Cancel"
              onDateChange={date => setDateEnd(date)}
              style={{ width: '100%' }}
              customStyles={{
                dateIcon: {
                  position: 'absolute',
                  left: 0,
                  top: 4,
                  marginLeft: 0
                },
                dateInput: {
                  marginLeft: 42
                }
              }}
            />
          </View>
        </Row>
        <Autocomplete
          label="Place"
          name="place"
          placeholder="Enter the place"
          style={Styles.space}
          data={geocoderData}
          onChangeText={searchAddress}
          value={geocoderQuery}
          renderItem={({ item }) => (
            <TouchableOpacity
              onPress={() => {
                setGeocoder(item);
                setGeocoderQuery(item.properties.label);
              }}
            >
              <Text>{item.properties.label}</Text>
            </TouchableOpacity>
          )}
        />
        <Button
          style={Styles.button}
          onPress={onValidatePressed}
          mode="contained"
          dark
        >
          Validate
        </Button>
      </ScrollView>
    </KeyboardAvoidingView>
  );
};

export default withNavigation(AddEventScreen);
