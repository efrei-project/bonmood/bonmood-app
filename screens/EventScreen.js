/* eslint-disable react/jsx-one-expression-per-line */
import React, { useState, useEffect } from 'react';
import {
  ScrollView,
  Image,
  ActivityIndicator,
  StyleSheet,
  View,
  Text,
  Alert,
} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';
import { withNavigation } from 'react-navigation';
import { Button, FAB } from 'react-native-paper';
import moment from 'moment';
import Row from '../components/Row';
import { useStateValue } from '../components/context/user_context';
import Colors from '../constants/Colors';
import Styles from '../constants/Styles';
import FetchApi from '../utils/ApiFetch';

const EventScreen = ({ navigation }) => {
  const [{ isAsso, isLogged, user }] = useStateValue();
  const [isParticipate, setParticipate] = useState(undefined);
  const [participant, setParticipant] = useState({
    number: 0
  });

  const event = navigation.getParam('event', undefined);
  // TODO: Create participating table:
  // const persons = `${event.persons} / ${event.totalPersons}`;
  const persons = `${participant.number} / ${event.number}`;

  useEffect(() => {
    if (isLogged) {
      getParticipant();
      checkParticipate();
    }
  }, [isLogged]);

  const getParticipant = async () => {
    const res = await FetchApi(`/events/${event.id}/participant`, 'GET');
    setParticipant({ number: res.length });
  };

  const checkParticipate = async () => {
    const res = await FetchApi(`/volunteers/${user.id}/events/${event.id}`, 'GET');
    setParticipate(res.isParticipating);
  };

  const addParticipate = async () => {
    await FetchApi(`/volunteers/${user.id}/events/${event.id}`, 'POST');
    await checkParticipate();
  };

  const deleteParticipate = async () => {
    await FetchApi(`/volunteers/${user.id}/events/${event.id}`, 'DELETE');
    await checkParticipate();
  };

  const showAlert = () => {
    Alert.alert('Ne plus participer', `Do you really want to cancel the mission ${event.name}`, [
      {
        text: 'Non',
      },
      {
        text: 'Oui',
        onPress: () => deleteParticipate()
      }
    ]);
  };

  const onParticipatePress = () => {
    if (!isLogged) {
      navigation.push('Login');
    } else if (isParticipate) {
      deleteParticipate();
    } else {
      addParticipate();
    }
  };

  const buildParticipateBtn = () => {
    if (isAsso) {
      return <View />;
    }

    if (isLogged && isParticipate === undefined) {
      return <ActivityIndicator size="large" color={Colors.lightPink} style={Styles.space} />;
    }
    if (isLogged && isParticipate) {
      return (
        <Button mode="contained" style={Styles.space} onPress={showAlert} dark>
          Ne plus participer
        </Button>
      );
    }
    return (
      <Button mode="contained" style={Styles.space} onPress={onParticipatePress} dark>
        Participer
      </Button>
    );
  };

  const deleteEventApi = async () => {
    try {
      await FetchApi(`/events/delete/${event.id}`, 'DELETE', {});
      navigation.navigate('MainTabNavigator');
      Alert.alert('Succès', 'Evenement correctement supprimé');
    } catch (e) {
      Alert.alert('Error', `${e}`);
    }
  };

  const deleteEvent = () => {
    Alert.alert(
      'Delete event',
      `Are you sure to delete this ${event.name} ?`,
      [
        {
          text: 'Cancel',
        },
        {
          text: 'Confirm',
          onPress: () => deleteEventApi()
        },
      ],
      {
        cancelable: false
      },
    );
  };

  return (
    <View style={Styles.container}>
      <ScrollView style={Styles.container}>
        <Image
          source={{ uri: event.addressImg }}
          style={Styles.headerImage}
          PlaceholderContent={<ActivityIndicator />}
          resizeMode="cover"
        />
        <View style={{ padding: 18 }}>

          <Row>
            <Icon name="people" size={30} style={styles.icon} />
            <Text>{persons}</Text>
          </Row>
          <Row justifyContent="space-between">
            <View>
              <Text style={styles.title}>Date begin</Text>
              <Text>{moment(event.date).format('MMMM Do YYYY')} at {moment(event.dateBegin).format('HH')}h</Text>
            </View>
            <View>
              <Text style={styles.title}>Date end</Text>
              <Text>{moment(event.date).format('MMMM Do YYYY')} at {moment(event.dateEnd).format('HH')}h</Text>
            </View>
          </Row>
          <Text style={[styles.space, styles.title]}>Place</Text>
          <Text>{event.place}</Text>
          <Text style={[styles.space, styles.title]}>Description</Text>
          <Text>{event.description}</Text>
          {buildParticipateBtn()}
        </View>
      </ScrollView>
      {isAsso ? (
        <View>
          <FAB
            style={Styles.fabEdit}
            icon="create"
            color="white"
          />
          <FAB
            style={Styles.fab}
            icon="delete"
            color="white"
            onPress={deleteEvent}
          />
        </View>
      ) : null}

    </View>
  );
};

const styles = StyleSheet.create({
  scrollview: {
    padding: 16.0
  },
  icon: {
    marginRight: 12.0
  },
  title: {
    fontWeight: '500',
  }
});

export default withNavigation(EventScreen);
