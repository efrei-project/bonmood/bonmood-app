import React from 'react';
import { StyleSheet, View, SafeAreaView } from 'react-native';
import { Button, Text } from 'react-native-paper';
import Colors from '../constants/Colors';
import { useStateValue } from '../components/context/user_context';

export default function HomeScreen({ navigation }) {
  const [, dispatch] = useStateValue();

  const onBenevol = () => {
    dispatch({ type: 'isAsso', value: false });
    navigation.navigate('MainTabNavigator');
  };

  const onAssociation = () => {
    dispatch({ type: 'isAsso', value: true });
    navigation.navigate('Login');
  };

  return (
    <SafeAreaView style={styles.container}>
      <View style={[styles.container, styles.centered, styles.header]}>
        <Text style={styles.title}>Welcome !</Text>
        <Text style={styles.subtitle}>You are ?</Text>
      </View>
      <View style={[styles.container, styles.centered]}>
        <Button style={styles.button} mode="contained" dark onPress={onBenevol}>
          Bénévole
        </Button>
        <Button style={styles.button} mode="contained" dark onPress={onAssociation}>
          Association
        </Button>
      </View>
      <View style={{ flex: 0.5 }} />
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  row: {
    flexDirection: 'row',
  },
  centered: {
    justifyContent: 'center',
    alignItems: 'center'
  },
  title: {
    fontSize: 42.0,
    width: '100%',
    textAlign: 'center',
    fontFamily: 'dosis-extrabold',
  },
  subtitle: {
    fontSize: 34.0,
  },
  button: {
    marginVertical: 16.0,
    width: 225,
    backgroundColor: Colors.deepPink
  },
  header: {
    paddingHorizontal: 32.0,
    paddingTop: 32.0
  }
});
