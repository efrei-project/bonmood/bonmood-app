/* eslint-disable react/no-unescaped-entities */
/* eslint-disable max-len */
import React, { useEffect } from 'react';
import {
  View,
  Image,
  Alert,
  ScrollView,
  KeyboardAvoidingView
} from 'react-native';
import {
  Title,
  Button,
} from 'react-native-paper';
import { useStateValue } from '../components/context/user_context';
import styles from '../constants/Styles';
import Row from '../components/Row';
import InputText from '../components/InputText';
import useInput from '../components/hooks/useInput';
import FetchApi from '../utils/ApiFetch';

const ProfileScreenAssociation = () => {
  const [{ user }, dispatch] = useStateValue();

  const name = useInput(user.name);
  const address = useInput(user.adresse);
  const phone = useInput(user.phone);
  const email = useInput(user.User.email);
  const description = useInput(user.description);

  const getUser = async (id) => {
    const response = await FetchApi(`/associations/${id}`);
    dispatch({ type: 'user', value: response });
  };

  const updateUser = async () => {
    const response = await FetchApi(`/associations/update/${user.id}`, 'PUT', {
      name: (name.value) ? name.value : '',
      address: (address.value) ? address.value : '',
      phone: (phone.value) ? phone.value : '',
      email: (email.value) ? email.value : '',
      description: (description.value) ? description.value : '',
    });

    if (response) {
      Alert.alert('Success', 'Your information were successfuly saved !');
    } else {
      Alert.alert('Error', 'An error has occured please try again !');
    }

    getUser(user.id);
  };

  return (
    <KeyboardAvoidingView style={styles.container} behavior="padding" enabled>
      <ScrollView style={styles.container}>
        <View style={styles.container}>
          <Row justifyContent="space-evenly">
            <Image
              source={{ uri: 'https://maia-asso.org/wp-content/uploads/rejoingnez-nous.png' }}
              style={{ width: 100, height: 100 }}
            />
            <Title>{user.name}</Title>
          </Row>


          <View>
            <InputText
              label="Name"
              name="name"
              placeholder="Enter you description"
              value={name.value}
              onChangeText={name.setValue}
            />
            <InputText
              label="Phone"
              name="phone"
              placeholder="Enter you phone number"
              value={phone.value}
              onChangeText={phone.setValue}
            />
            <InputText
              label="Email"
              name="email"
              placeholder=""
              value={email.value}
              onChangeText={email.setValue}
            />
            <InputText
              label="Address"
              name="address"
              placeholder=""
              value={address.value}
              onChangeText={address.setValue}
            />
            <InputText
              multiline
              numberOfLines={5}
              label="Description"
              name="description"
              placeholder="Enter your description"
              value={description.value}
              onChangeText={description.setValue}
            />
          </View>
          <Button
            style={[styles.button, { marginBottom: 16.0 }]}
            onPress={updateUser}
            mode="contained"
            dark
          >
            Save
          </Button>
        </View>
      </ScrollView>
    </KeyboardAvoidingView>
  );
};

export default ProfileScreenAssociation;
