/* eslint-disable react/jsx-one-expression-per-line */
import React, { useEffect, useState } from 'react';
import {
  View,
  Text,
  Image,
  ScrollView,
  ActivityIndicator
} from 'react-native';
import { Appbar, Paragraph, Title } from 'react-native-paper';
import moment from 'moment';
import Icon from 'react-native-vector-icons/Entypo';
import { withNavigation } from 'react-navigation';
import { useStateValue } from '../components/context/user_context';
import AppBar from '../components/AppBar';
import Row from '../components/Row';
import styles from '../constants/Styles';
import Colors from '../constants/Colors';
import FetchApi from '../utils/ApiFetch';

const UserScreen = ({ navigation }) => {
  const [{ user, isLogged }] = useStateValue();
  const [participation, setParticipation] = useState({
    number: 0,
    events: []
  });
  const redirect = () => {
    const delay = ms => new Promise(res => setTimeout(res, ms));
    delay(2000);
    navigation.navigate('Login');
  };

  useEffect(() => {
    getParticipation();
  }, [isLogged]);

  const getParticipation = async () => {
    const res = await FetchApi(`/volunteers/${user.id}/events`, 'GET');
    setParticipation({ number: res.length, events: res });
  };

  const { events } = participation;

  if (!isLogged) {
    redirect();
    return (
      <View style={styles.container}>
        <View style={styles.container} />
        <View style={styles.center}>
          <Image
            source={require('../assets/images/sad.png')}
            style={{ width: 100, height: 100 }}
          />
        </View>
        <View style={styles.center}>
          <Title>
            You are not logged,
          </Title>
          <Title>
            you will be redirected
               to the login page
          </Title>
        </View>
        <View style={styles.center}>
          <ActivityIndicator size="large" color={Colors.lightPink} />
        </View>
      </View>
    );
  }

  const {
    birthday,
    firstname,
    lastname,
    User
  } = user;

  return (
    <View style={styles.container}>
      <AppBar title="Profile">
        <Appbar.Action icon="create" />
      </AppBar>
      <Row justifyContent="space-evenly">
        <Image
          source={{ uri: 'http://pinkplumbingservices.co.uk/wp-content/uploads/2016/08/profile-1.png' }}
          style={{ width: 80, height: 80 }}
        />
        <View>
          <Title>{firstname} {lastname}</Title>
          <View>
            <Paragraph>{moment(birthday).diff(moment(), 'years')} years</Paragraph>
            <Paragraph>{User.email}</Paragraph>
          </View>
        </View>
      </Row>
      <Text>Nombre de participation : {participation.number}</Text>
      <ScrollView>
        <Title>Preferences</Title>
        <View style={styles.textComponent}>
          <View>
            <Title>Tags</Title>
            <Row justifyContent="space-evenly">
              <View alignItems="center">
                <Icon name="leaf" size={20} />
                <Text>Nature</Text>
              </View>
              <View alignItems="center">
                <Icon name="baidu" size={20} />
                <Text>Animals</Text>
              </View>
              <View alignItems="center">
                <Icon name="globe" size={20} />
                <Text>Humanitarian</Text>
              </View>
            </Row>
          </View>
          <View>
            <Title>Associations</Title>
            <Row justifyContent="space-evenly">
              <Icon name="image-inverted" size={30} />
              <Icon name="image-inverted" size={30} />
              <Icon name="image-inverted" size={30} />
              <Icon name="image-inverted" size={30} />
            </Row>
          </View>
        </View>
        <Title>Actions effectuees</Title>
        <View style={styles.textComponent}>
          {events.map(info => (
            <Row justifyContent="space-between" alignItems="center">
              <Image
                source={{ uri: info.addressImg }}
                style={{ height: 50, width: 50 }}
              />
              <Text>{info.name}</Text>
            </Row>
          ))}
        </View>
      </ScrollView>
    </View>
  );
};

export default withNavigation(UserScreen);
