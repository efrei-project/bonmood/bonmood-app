import React, { useState, useEffect } from 'react';
import {
  ScrollView, View, Text, RefreshControl,
} from 'react-native';
import { withNavigation } from 'react-navigation';
import {
  ActivityIndicator, Headline, FAB, IconButton
} from 'react-native-paper';
import EventRow from '../components/EventRow';
import FetchApi from '../utils/ApiFetch';
import styles from '../constants/Styles';
import { useStateValue } from '../components/context/user_context';
import Colors from '../constants/Colors';

const HomeScreen = ({ navigation }) => {
  const [{ isAsso }] = useStateValue();

  const [fetchState, setFetchState] = useState({
    isLoading: true,
    events,
    error,
  });

  useEffect(() => {
    fetchData();
  }, []);

  const fetchData = async () => {
    setFetchState({ isLoading: true, events });
    try {
      const events = await FetchApi('/events');
      setFetchState({ isLoading: false, events });
    } catch (error) {
      setFetchState({ isLoading: false, error });
    }
  };

  const { isLoading, events, error } = fetchState;

  if (isLoading && !events) {
    return <ActivityIndicator size="large" color={Colors.lightPink} style={styles.center} />;
  }
  if (error) {
    // eslint-disable-next-line react/jsx-one-expression-per-line
    return <Text>`Error when try to get events... ${JSON.stringify(error)}`</Text>;
  }

  return (
    <View style={styles.container}>
      {events.length === 0
        ? (
          <View style={styles.center}>
            <Headline>No events found :/</Headline>
            <IconButton icon="refresh" onPress={fetchData} />
          </View>
        )
        : (
          <ScrollView
            style={styles.container}
            refreshControl={<RefreshControl refreshing={isLoading} onRefresh={fetchData} />}
          >
            {events.map(e => <EventRow event={e} key={e.id} />)}
          </ScrollView>
        )
      }
      {isAsso
        ? (
          <FAB
            style={styles.fab}
            icon="add"
            color="white"
            onPress={() => navigation.push('AddEvent')}
          />
        )
        : (
          <FAB
            style={styles.fab}
            icon="filter-list"
            color="white"
            onPress={() => navigation.push('MainTabNavigator')}
          />
        )}
    </View>
  );
};

export default withNavigation(HomeScreen);
