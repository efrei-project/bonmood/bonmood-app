/* eslint-disable global-require */
import React, { useState, useCallback } from 'react';
import {
  View,
  Text,
  Image,
  KeyboardAvoidingView,
} from 'react-native';
import { TabView, TabBar } from 'react-native-tab-view';
import LoginForm from '../components/LoginForm';
import RegisterForm from '../components/RegisterForm';
import Layout from '../constants/Layout';
import styles from '../constants/Styles';


const LoginScreen = () => {
  const [navState, setNavState] = useState({
    index: 0,
    routes: [
      { key: 'login', title: 'Login' },
      { key: 'register', title: 'Register' },
    ],
  });

  const renderScene = useCallback(({ route }) => {
    switch (route.key) {
      case 'login':
        return <LoginForm />;
      case 'register':
        return <RegisterForm />;
      default:
        return null;
    }
  });

  const renderTabBar = props => (
    <TabBar
      {...props}
      labelStyle={styles.tablabel}
      indicatorStyle={styles.tabindicator}
      style={styles.tabbar}
    />
  );

  return (
    <KeyboardAvoidingView style={styles.container} behavior="padding" enabled>
      <View style={styles.header}>
        <Image
          source={require('../assets/images/bonmood_trp.png')}
          resizeMode="contain"
          style={styles.image}
        />
        <Text style={styles.title}>To acces you have to login or register</Text>
      </View>
      <TabView
        navigationState={navState}
        renderScene={renderScene}
        onIndexChange={index => setNavState({ ...navState, index })}
        initialLayout={{ width: Layout.window.width }}
        renderTabBar={renderTabBar}
      />
    </KeyboardAvoidingView>
  );
};

export default LoginScreen;
