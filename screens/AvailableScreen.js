import React, { useState } from 'react';
import { ScrollView, View } from 'react-native';
import {
  Button,
  Dialog,
  Portal,
  FAB
} from 'react-native-paper';
import { withNavigation } from 'react-navigation';
import AvailabilityData from '../constants/Available';
import AvailabilityRow from '../components/AvailabilityRow';
import AvailabilityDialog from '../components/AvailabilityDialog';
import styles from '../constants/Styles';

const AvailableScreen = () => {
  const [visible, setVisible] = useState();

  const showDialog = () => setVisible(true);
  const hideDialog = () => setVisible(false);

  return (
    <View style={styles.container}>
      <ScrollView style={styles.container}>
        {AvailabilityData.map(e => <AvailabilityRow available={e} key={e.id} />)}
        <Portal>
          <Dialog visible={visible}>
            <AvailabilityDialog />
            <Dialog.Actions>
              <Button onPress={hideDialog}>Cancel</Button>
              <Button onPress={hideDialog}>Done</Button>
            </Dialog.Actions>
          </Dialog>
        </Portal>
      </ScrollView>
      <FAB
        style={styles.fab}
        icon="add"
        color="white"
        onPress={showDialog}
      />
    </View>
  );
};

export default withNavigation(AvailableScreen);
