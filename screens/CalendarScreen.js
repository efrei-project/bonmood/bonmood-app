/* eslint-disable max-len */
import React, { useState, useEffect } from 'react';
import { View, StyleSheet } from 'react-native';
import { withNavigation } from 'react-navigation';
import { Calendar } from 'react-native-calendars';
import moment from 'moment';
import { useStateValue } from '../components/context/user_context';
import Colors from '../constants/Colors';
import FetchApi from '../utils/ApiFetch';

const CalendarScreen = ({ navigation }) => {
  const [{ isAsso, user, isLogged }] = useStateValue();
  const [events, setEvents] = useState([]);

  useEffect(() => {
    if (!isLogged) {
      navigation.push('Login');
    }
    getMyEvents();
  });

  const getMyEvents = async () => {
    const baseUrl = (isAsso) ? '/associations' : '/volunteers';
    const myEvents = await FetchApi(`${baseUrl}/${user.id}/events`);

    setEvents(myEvents);
  };

  const formattedEvents = events.map(e => Object({
    date: moment(e.date).format('YYYY-MM-DD'),
    event: e
  }));
  let marked = {};

  formattedEvents.forEach((event) => {
    marked = {
      ...marked,
      [moment(event.date).format('YYYY-MM-DD')]: { marked: true, dotColor: Colors.deepPink }
    };
  });

  return (
    <View style={styles.container}>
      <Calendar
        current={moment().format('YYYY-MM-DD')}
        minDate="2019-05-01"
        maxDate={moment().add(2, 'years').format('YYYY-MM-DD')}
        onDayPress={(day) => {
          formattedEvents.forEach((e) => {
            if (day.dateString === e.date) {
              navigation.navigate('EventScreen', { event: e.event });
            }
          });
        }}
        monthFormat="MMMM yyyy"
        firstDay={1}
        markedDates={marked}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: 50,
  },
});

export default withNavigation(CalendarScreen);
